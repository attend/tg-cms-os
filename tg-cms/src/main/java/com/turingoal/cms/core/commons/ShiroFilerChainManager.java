package com.turingoal.cms.core.commons;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.apache.shiro.web.filter.mgt.DefaultFilterChainManager;
import org.apache.shiro.web.filter.mgt.NamedFilterList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.turingoal.cms.core.domain.Resource;

/**
 * 在进行新增、修改、删除资源时会调用initFilterChain来重新初始化Shiro的URL拦截器链，即同步数据库中的URL拦截器定义到Shiro中。 此处也要注意如果直接修改数据库是不会起作用的，因为只要调用这几个Service方法时才同步。另外当容器启动时会自动回调initFilterChain来完成容器启动后的URL拦截器的注册。
 */
@Service
public class ShiroFilerChainManager {

	@Autowired
	private DefaultFilterChainManager filterChainManager;

	private Map<String, NamedFilterList> defaultFilterChains;

	/**
	 * Spring容器启动时会调用init方法把在spring配置文件中配置的默认拦截器保存下来，之后会自动与数据库中的配置进行合并。
	 */
	@PostConstruct
	public void init() {
		defaultFilterChains = new HashMap<String, NamedFilterList>(filterChainManager.getFilterChains());
	}

	/**
	 * UrlFilterServiceImpl会在Spring容器启动或进行增删改UrlFilter时进行注册URL拦截器到Shiro。
	 */
	public void initFilterChains(final List<Resource> urlFilters) {
		// 1、首先删除以前老的filter chain并注册默认的
		filterChainManager.getFilterChains().clear();
		if (defaultFilterChains != null) {
			filterChainManager.getFilterChains().putAll(defaultFilterChains);
		}
		// 2、循环URL Filter 注册filter chain
		for (Resource urlFilter : urlFilters) {
			String url = urlFilter.getPermValue();

			// 注册roles filter
			// if (!StringUtils.isEmpty(urlFilter.getRoles())) {
			// filterChainManager.addToChain(url, "roles", urlFilter.getRoles());
			// }
			// 注册perms filter
			String permission = urlFilter.getPermission();
			if (!StringUtils.isEmpty(url) && !StringUtils.isEmpty(permission)) {
				filterChainManager.addToChain(url, "perms", permission);
			}
		}
	}
}
