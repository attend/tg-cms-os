package com.turingoal.cms.core.commons;

import org.apache.shiro.crypto.hash.SimpleHash;
import com.turingoal.common.util.io.PropsUtil;

/**
 * 定义一个自定义的PasswordEncoder，从而加强应用的安全认证和高安全性。 使用shiro的RSA-256加密
 */
public final class ShiroPasswordHelper {
	private static String applicationSalt = PropsUtil.getValue("shiro.applicationSalt", "application.properties");
	private static String hashAlgorithmName = PropsUtil.getValue("shiro.hashAlgorithmName", "application.properties");
	private static int hashIterations = Integer.valueOf(PropsUtil.getValue("shiro.hashIterations", "application.properties"));
	private static boolean storedCredentialsHexEncoded = Boolean.valueOf(PropsUtil.getValue("shiro.storedCredentialsHexEncoded", "application.properties"));

	private ShiroPasswordHelper() {
		throw new Error("工具类不能实例化！");
	}

	/**
	 * 将输入的密码进行特殊处理，防止密码轻易被破解，增强应用的安全性 密码 + 常量 +用户名
	 * 
	 * @param password
	 *            需要加密的字符串
	 * @param username
	 *            用户名
	 * @param salt
	 *            加密盐
	 * @return 返回加密后的密码字符串
	 */
	public static String encodePassword(final String password, final String username, final String salt) {
		if (storedCredentialsHexEncoded) {
			return new SimpleHash(hashAlgorithmName, password, applicationSalt + salt + username, hashIterations).toHex();
		} else {
			return new SimpleHash(hashAlgorithmName, password, applicationSalt + salt + username, hashIterations).toString();
		}
	}

	/**
	 * 判断输入的密码是否与应用中存储的密码相符合。因为应用中存储的密码是由输入的密码经过特殊处理后生成的， 所以需要我们自己定义如何判断输入的密码和存储的密码的一致性
	 * 
	 * @param encPass
	 *            加密后的字符串
	 * @param password
	 *            需要加密的字符串
	 * @param username
	 *            用户名
	 * @param userSalt
	 *            加密盐
	 * @return 加密后的字符串和传入的字符串是否相同
	 */
	public static boolean isPasswordValid(final String encPass, final String password, final String username, final String userSalt) {
		if (storedCredentialsHexEncoded) {
			return new SimpleHash(hashAlgorithmName, password, applicationSalt + userSalt + username, hashIterations).toHex().equals(encPass);
		} else {
			return new SimpleHash(hashAlgorithmName, password, applicationSalt + userSalt + username, hashIterations).toString().equals(encPass);
		}
	}
}
