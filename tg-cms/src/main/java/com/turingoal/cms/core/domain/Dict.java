package com.turingoal.cms.core.domain;

import java.io.Serializable;
import lombok.Data;

/**
 * Dict
 */
@Data
public class Dict implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id; // 数据字典
	private String dictType; // 字典类型
	private String dictName; // 字典名称
	private String dictValue; // 字典值
	private Integer editable; // 是否可编辑 1可编辑 2不可编辑
	private String description; // 描述
	private Integer priority; // 排序
	private Integer state; // 状态 1启用 2停用
}