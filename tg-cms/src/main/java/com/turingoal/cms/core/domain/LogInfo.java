package com.turingoal.cms.core.domain;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * Loginfo
 */
@Data
public class LogInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id; // 操作日志
	private String logType; // 类型(操作日志,登录日志)
	private String message; //
	private String exception; //
	private String ipAddress; // IP
	private Date eventDate; // 时间
	private String username; // 站点
}