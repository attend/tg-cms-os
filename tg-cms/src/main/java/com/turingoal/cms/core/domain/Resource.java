package com.turingoal.cms.core.domain;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.turingoal.common.bean.BaseTreeNodeBean;

/**
 * Resource
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Resource extends BaseTreeNodeBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String resourceName; // 名称
	private String iconCls;
	private Integer type; // 资源类型 1菜单 2按钮 3方法
	private String code; // 权限代码
	private String permission; // 权限标识
	private String permValue;
	private String description; // 描述
	private boolean expanded = true; // 是否展开
	private Integer priority; // 优先级
	private Integer editable; // 是否可编辑 1可编辑 2不可编辑
	private Integer available; // 是否可用
	private String roleId;
	private String codeNum;
}