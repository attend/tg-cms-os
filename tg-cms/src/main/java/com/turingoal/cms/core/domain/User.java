package com.turingoal.cms.core.domain;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 用户
 */
@Data
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id; // 用户表
	private String username; // 用户名
	private String realname; // 真实姓名
	private String userPass; // 密码
	private String userSalt; // 加密盐
	private String idCardNum; // 身份证号
	private Integer gender; // 性别
	private Date birthday; // 出生日期
	private String cellphoneNumber; // 手机号
	private String telephoneNumber; // 电话号码
	private String email; // 邮箱
	private Date preLoginTime; // 上次登录时间
	private String preLoginIp; // 上次登录ip
	private Date lastLoginTime; // 最后登录时间
	private String lastLoginIp; // 最后登录ip
	private Date lastLogoutTime; // 最后登出时间
	private Integer locked; // 是否锁定
	private Integer expired; // 是否禁用
	private Integer disabled; //
	private Integer editable = 1; // 是否可编辑 1可编辑 2不可编辑
	private Integer available = 1; // 是否可用
}