package com.turingoal.cms.core.domain.query;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.turingoal.common.bean.BaseQueryBean;

/**
 * DbBackupQuery 
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DbBackupQuery extends BaseQueryBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id; // 数据库备份
	private Date backupDate; // 备份时间
	private String description; // 描述
}