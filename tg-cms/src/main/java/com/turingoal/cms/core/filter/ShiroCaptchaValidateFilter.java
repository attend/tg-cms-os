package com.turingoal.cms.core.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;
import com.turingoal.cms.core.commons.SystemHelper;
import com.turingoal.common.bean.JsonResultBean;
import com.turingoal.common.util.io.JsonUtil;
import com.turingoal.common.util.math.CaptchaUtil;

/**
 * ShiroCaptchaValidateFilter
 * 
 */
public class ShiroCaptchaValidateFilter extends AccessControlFilter {
	// private Cache<String, AtomicInteger> passwordRetryCache;
	private boolean captchaEbabled = true; // 是否开启验证码支持
	private String captchaParam = "captchaCode"; // 前台提交的验证码参数名
	private int showCaptchaRetryCount = 3; // 验证出错次数显示验证码
	private String failureKeyAttribute = "shiroLoginFailure"; // 验证失败后存储到的属性名

	// public void setCacheManager(final CacheManager cacheManager) {
	// this.passwordRetryCache = cacheManager.getCache("shiro-passwordRetryCache");
	// }

	public void setCaptchaEbabled(final boolean captchaEbabledParm) {
		this.captchaEbabled = captchaEbabledParm;
	}

	public void setCaptchaParam(final String captchaP) {
		this.captchaParam = captchaP;
	}

	public void setShowCaptchaRetryCount(final int showCaptchaRetryCountParm) {
		this.showCaptchaRetryCount = showCaptchaRetryCountParm;
	}

	public void setFailureKeyAttribute(final String failureKeyAttributeParm) {
		this.failureKeyAttribute = failureKeyAttributeParm;
	}

	@Override
	protected boolean isAccessAllowed(final ServletRequest request, final ServletResponse response, final Object mappedValue) throws Exception {
		// 设置验证码是否开启属性，页面可以根据该属性来决定是否显示验证码
		request.setAttribute("captchaEbabled", captchaEbabled);
		HttpServletRequest httpServletRequest = WebUtils.toHttp(request);
		// String username = httpServletRequest.getParameter("username");
		// 判断验证码是否禁用 或不是表单提交（允许访问）
		if (!captchaEbabled || !"post".equalsIgnoreCase(httpServletRequest.getMethod())) {
			return true;
		}
		AtomicInteger sessionRetryCount = (AtomicInteger) SystemHelper.getSession().getAttribute("sessionRetryCount");
		int sessionErrorCount = 0;
		if (sessionRetryCount != null) {
			sessionErrorCount = sessionRetryCount.get();
		}
		if (captchaEbabled && sessionErrorCount >= showCaptchaRetryCount) { // 验证验证码是否正确
			if (CaptchaUtil.checkCaptcha(WebUtils.toHttp(request).getSession(), httpServletRequest.getParameter(captchaParam))) {
				return true;
			} else {
				// 重置验证码防止客户端恶意提交
				CaptchaUtil.resetCaptcha(httpServletRequest.getSession());
				try {
					response.setCharacterEncoding("utf-8");
					PrintWriter writer = response.getWriter();
					writer.write(JsonUtil.object2Json(new JsonResultBean(JsonResultBean.FAULT, "验证码错误")));
					writer.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return false;
			}
		} else {
			return true;
		}
	}

	@Override
	protected boolean onAccessDenied(final ServletRequest request, final ServletResponse response) throws Exception {
		// 如果验证码失败了，存储失败key属性
		request.setAttribute(failureKeyAttribute, "captcha.error");
		return false;
	}
}
