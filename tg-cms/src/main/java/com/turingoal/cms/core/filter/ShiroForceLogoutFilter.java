package com.turingoal.cms.core.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.session.Session;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;

/**
 * ForceLogoutFilter
 */
public class ShiroForceLogoutFilter extends AccessControlFilter {
	private final Logger log = LogManager.getLogger(ShiroForceLogoutFilter.class);

	@Override
	protected boolean isAccessAllowed(final ServletRequest request, final ServletResponse response, final Object mappedValue) throws Exception {
		Session session = getSubject(request, response).getSession(false);
		if (session == null) {
			return true;
		}
		return session.getAttribute("session.force.logout") == null;
	}

	@Override
	protected boolean onAccessDenied(final ServletRequest request, final ServletResponse response) throws Exception {
		try {
			getSubject(request, response).logout(); // 强制退出
		} catch (Exception e) {
			/* ignore exception */
			log.warn("出异常了");
		}
		String loginUrl = getLoginUrl() + (getLoginUrl().contains("?") ? "&" : "?") + "forceLogout=1";
		WebUtils.issueRedirect(request, response, loginUrl);
		return false;
	}
}
