package com.turingoal.cms.core.filter;

import java.io.Serializable;
import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.DefaultSessionKey;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;

/**
 * 如果同时有多人登录：要么不让后者登录；要么踢出前者登录）。Shiro的话没有提供默认实现，通过过滤器实现
 */
public class ShiroKickoutSessionControlFilter extends AccessControlFilter {
	private final Logger log = LogManager.getLogger(ShiroKickoutSessionControlFilter.class);
	private String kickoutUrl; // 踢出后到的地址
	private boolean kickoutAfter = false; // 踢出之前登录的/之后登录的用户 默认踢出之前登录的用户
	private int maxSession = 1; // 同一个帐号最大会话数 默认1
	private SessionManager sessionManager;
	private Cache<String, Deque<Serializable>> cache;

	public void setKickoutUrl(final String kickoutUrlParm) {
		this.kickoutUrl = kickoutUrlParm;
	}

	public void setKickoutAfter(final boolean kickoutAfterParm) {
		this.kickoutAfter = kickoutAfterParm;
	}

	public void setMaxSession(final int maxSessionParm) {
		this.maxSession = maxSessionParm;
	}

	public void setSessionManager(final SessionManager sessionManagerParm) {
		this.sessionManager = sessionManagerParm;
	}

	public void setCacheManager(final CacheManager cacheManager) {
		this.cache = cacheManager.getCache("shiro-kickout-session");
	}

	@Override
	protected boolean isAccessAllowed(final ServletRequest request, final ServletResponse response, final Object mappedValue) throws Exception {
		return false;
	}

	@Override
	protected boolean onAccessDenied(final ServletRequest servletRequest, final ServletResponse servletResponse) throws Exception {
		HttpServletRequest request = WebUtils.toHttp(servletRequest);
		HttpServletResponse response = WebUtils.toHttp(servletResponse);
		Subject subject = getSubject(request, response);
		if (!subject.isAuthenticated() && !subject.isRemembered()) {
			// 如果没有登录，直接进行之后的流程
			return true;
		}
		Session session = subject.getSession();
		String username = (String) subject.getPrincipal();
		Serializable sessionId = session.getId();

		synchronized (this.cache) {
			Deque<Serializable> deque = this.cache.get(username);
			if (deque == null) {
				deque = new ConcurrentLinkedDeque<Serializable>();
			}
			// 如果队列里没有此sessionId，且用户没有被踢出；放入队列
			if (!deque.contains(sessionId) && session.getAttribute("kickOut") == null) {
				session.setAttribute("userName", username);
				deque.addLast(sessionId);
			}
			log.debug("logged user:" + username + ", deque size = " + deque.size());
			Serializable kickOutSessionId = null;
			// 如果队列里的sessionId数超出最大会话数，开始踢人
			if (deque.size() > maxSession) {
				if (!this.kickoutAfter) { // 如果踢出前者
					kickOutSessionId = deque.removeFirst();
					log.debug("踢出最先的 session: " + kickOutSessionId);
				} else {
					kickOutSessionId = deque.removeLast();
					log.debug("踢出后来的 session: " + kickOutSessionId);
				}
			}
			try {
				Session kickoutSession = sessionManager.getSession(new DefaultSessionKey(kickOutSessionId));
				if (kickoutSession != null) {
					// 设置会话的kickout属性表示踢出了
					kickoutSession.setAttribute("kickout", true);
					// ext用 添加状态码 让ext自动跳转
				}
			} catch (Exception e) { // ignore exception
				log.error(e.getMessage());
			}
			this.cache.put(username, deque);
		}

		// 如果被踢出了，直接退出，重定向到踢出后的地址
		if (session.getAttribute("kickout") != null) {
			log.info("账号:{} 在其它地方登陆，被提下 。sessionId:{} ", username, sessionId);
			// 会话被踢出了
			try {
				subject.logout();
			} catch (Exception e) { // ignore
				log.error(e.getMessage());
			}
			// extjs 存入状态码
			String type = request.getHeader("X-Requested-With");
			if (type != null && type.equalsIgnoreCase("XMLHttpRequest")) {
				// if (SystemHelper.getCurrentUser() == null) {
				response.setHeader("forceLogout", "lougout");
				// }
				saveRequest(request);
			} else {
				saveRequest(request);
				WebUtils.issueRedirect(request, response, kickoutUrl);
			}
			return false;
		}
		return true;
	}
}
