package com.turingoal.cms.core.filter;

import java.io.Serializable;
import java.util.Deque;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;
import org.apache.shiro.subject.Subject;

/**
 * ShiroSessionListener
 */
public class ShiroSessionListener implements SessionListener {
	private final Logger log = LogManager.getLogger(ShiroSessionListener.class);
	private Cache<String, Deque<Serializable>> cache;

	public void setCacheManager(final CacheManager cacheManager) {
		this.cache = cacheManager.getCache("kickOutSessionCache");
	}

	/**
	 * 从缓存移除session
	 */
	private void removeSessionFromCache(final Session session) {
		Subject subject = SecurityUtils.getSubject();
		String username = (String) subject.getPrincipal();
		if (username != null) {
			this.log.debug("remove session: " + session.getId() + " in deque of " + username + "@kickOutSessionCache");
			synchronized (this.cache) {
				Deque<Serializable> deque = this.cache.get(username);
				deque.remove(session.getId());
				this.cache.put(username, deque);
			}
		}
	}

	@Override
	public void onStop(final Session session) {
		this.removeSessionFromCache(session);
	}

	@Override
	public void onExpiration(final Session session) {
		this.removeSessionFromCache(session);
	}

	@Override
	public void onStart(final Session session) {}

}
