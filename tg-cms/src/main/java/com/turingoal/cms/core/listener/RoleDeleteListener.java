package com.turingoal.cms.core.listener;

/**
 * ModelDeleteListener
 */
public interface RoleDeleteListener {
	/**
	 * preRoleDelete
	 */
	void preRoleDelete(Integer[] ids);
}
