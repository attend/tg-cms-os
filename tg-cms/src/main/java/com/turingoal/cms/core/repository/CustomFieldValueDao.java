package com.turingoal.cms.core.repository;

import java.util.List;
import java.util.Map;
import com.turingoal.cms.core.domain.CustomFieldValue;
import com.turingoal.cms.core.domain.form.CustomFieldValueForm;
import com.turingoal.cms.core.domain.query.CustomFieldValueQuery;

/**
 * CustomFieldValueDao
 */
public interface CustomFieldValueDao {

	/**
	 * 查询全部 CustomFieldValue
	 */
	List<CustomFieldValue> find(final CustomFieldValueQuery query);

	/**
	 * 通过id得到一个 CustomFieldValue
	 */
	CustomFieldValue get(final String id);

	/**
	 * 新增 CustomFieldValue
	 */
	void add(final CustomFieldValueForm form);

	/**
	 * 修改 CustomFieldValue
	 */
	int update(final CustomFieldValueForm form);

	/**
	 * 根据id删除一个 CustomFieldValue
	 */
	int delete(final String id);

	/**
	 * 根据id删除多个 CustomFieldValue
	 */
	int deleteAll(final String ids);

	/**
	 * 修改状态
	 */
	int changeState(final Map<String, Object> map);

	/**
	 * 根据model删除用户自定义值
	 */
	int deleteByModel(Map<String, Object> params);

	/**
	 * findByOwner
	 */
	List<CustomFieldValue> findByOwner(final CustomFieldValueQuery query);
}