package com.turingoal.cms.core.repository;

import java.util.List;
import java.util.Map;
import com.turingoal.cms.core.domain.DbBackup;
import com.turingoal.cms.core.domain.form.DbBackupForm;

/**
 * 数据库备份Dao
 */
public interface DbBackupDao {

	/**
	 * 获得Schema
	 */
	String getSchema();

	/**
	 * 获得所有表名
	 */
	List<String> getTables(String schema);

	/**
	 * 获得TableDDL
	 */
	Map<String, String> getTableDDL(Map<String, String> map);

	/**
	 * getTableData
	 */
	List<Map<String, Object>> getTableData(Map<String, String> map);

	/**
	 * 查询全部 DbBackup
	 */
	List<DbBackup> find();

	/**
	 * 新增 DbBackup
	 */
	void add(final DbBackupForm form);

	/**
	 * 根据id删除一个 DbBackup
	 */
	int delete(final String id);
}
