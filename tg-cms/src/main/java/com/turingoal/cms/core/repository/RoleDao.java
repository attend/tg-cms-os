package com.turingoal.cms.core.repository;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.turingoal.cms.core.domain.Role;
import com.turingoal.cms.core.domain.form.RoleForm;

/**
 * 角色Dao
 */
public interface RoleDao {

	/**
	 * 查询全部角色
	 */
	List<Role> find();

	/**
	 * 查询全部启用的角色
	 */
	List<Role> findEnabled();

	/**
	 * 通过id得到一个角色
	 */
	Role get(final String id);

	/**
	 * 新增角色
	 */
	void add(final RoleForm form);

	/**
	 * 修改角色
	 */
	int update(final RoleForm form);

	/**
	 * 根据id删除一个角色
	 */
	int delete(final String id);

	/**
	 * 根据id删除多个角色
	 */
	int deleteAll(final String ids);

	/**
	 * 修改是否可用
	 */
	int changeAvailable(@Param("id") final String id, @Param("available") final Integer available);

	/**
	 * 检测数据是否可编辑
	 */
	int checkEditable(final String id);

	/**
	 * 得到最大优先级
	 */
	int getMaxPriority(final RoleForm form);

	/**
	 * 修改优先级
	 */
	int changePriority(final RoleForm form);

	/**
	 * 递增优先级
	 */
	int increasePrioritys(final RoleForm form);

	/**
	 * 递减优先级
	 */
	int decreasePrioritys(final RoleForm form);

	/**
	 * 删除数据，更新其它数据优先级
	 */
	int changePrioritysByDelete(final String id);
}