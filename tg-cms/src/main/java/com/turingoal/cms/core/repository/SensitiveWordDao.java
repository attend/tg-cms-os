package com.turingoal.cms.core.repository;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.turingoal.cms.core.domain.SensitiveWord;
import com.turingoal.cms.core.domain.form.SensitiveWordForm;
import com.turingoal.cms.core.domain.query.SensitiveWordQuery;

/**
 * SensitiveWordDao
 */
public interface SensitiveWordDao {
	/**
	 * 查询全部 启用的SensitiveWord
	 */
	List<SensitiveWord> findEnabled();

	/**
	 * 查询 SensitiveWord
	 */
	List<SensitiveWord> find(final SensitiveWordQuery query);

	/**
	 * 通过id得到一个 SensitiveWord
	 */
	SensitiveWord get(final String id);

	/**
	 * 新增 SensitiveWord
	 */
	void add(final SensitiveWordForm form);

	/**
	 * 修改 SensitiveWord
	 */
	int update(final SensitiveWordForm form);

	/**
	 * 根据id删除一个 SensitiveWord
	 */
	int delete(final String id);

	/**
	 * 根据id删除多个 SensitiveWord
	 */
	int deleteAll(final String ids);

	/**
	 * 修改是否可用
	 */
	int changeAvailable(@Param("id") final String id, @Param("available") final Integer available);
}