package com.turingoal.cms.core.service;

import java.util.Map;

/**
 * 系统配置Service
 */
public interface ConfigSysService {

	/**
	 * 取配置文件里的值
	 */
	String getPropValue(final String key);

	/**
	 * 设置配置文件里的值
	 */
	void setPropValue(final String key, final Object value);

	/**
	 * 得到系统全局配置
	 */
	Map<String, Object> getSystemConfig();

	/**
	 * 得到用户登录配置
	 */
	Map<String, Object> getLoginConfig();

	/**
	 * 修改用户登录配置
	 */
	boolean updateLoginConfig(final Map<String, Object> config);

	/**
	 * 是否显示验证码
	 */
	boolean isCheckCodeShow();

	/**
	 * 显示验证码的最小次数
	 */
	int getCheckCodeShowNum();

	/**
	 * 是否出错超过次数锁定
	 */
	boolean isErrosLock();

	/**
	 * 出错次数账号锁定
	 */
	int getLockNum();

	/**
	 * 密码错误失效时间
	 */
	int getErrorBreakTime();

	/**
	 * 锁定时间
	 */
	int getLockTime();
}
