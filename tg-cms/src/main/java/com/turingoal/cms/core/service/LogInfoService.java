package com.turingoal.cms.core.service;

import com.github.pagehelper.Page;
import com.turingoal.cms.core.domain.LogInfo;
import com.turingoal.cms.core.domain.query.LogInfoQuery;

/**
 * 操作日志Service
 */
public interface LogInfoService {

	/**
	 * 分页查询 Loginfo
	 */
	Page<LogInfo> findByPage(final LogInfoQuery query);
}