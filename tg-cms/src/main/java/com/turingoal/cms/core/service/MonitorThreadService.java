package com.turingoal.cms.core.service;

import com.turingoal.cms.core.domain.MoitorThreadinfoResult;

/**
 * 线程信息
 */
public interface MonitorThreadService {

	/**
	 * 线程信息
	 */
	MoitorThreadinfoResult getInfo();
}
