package com.turingoal.cms.core.service.impl;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.turingoal.cms.core.commons.SystemHelper;
import com.turingoal.cms.core.service.ConfigCustomService;

/**
 * 个性化定制
 */
@Service
public class ConfigCustomServiceImpl implements ConfigCustomService {
	public static final String PROP_FILE_NAME = "application.properties";
	/** 读取配置文件的值，分号后面为没有此配置项时的默认值 */
	@Value("${customer.prjNoPrefix:'YTSJ'}")
	private String prjNoPrefix;
	@Value("${customer.loginTtile:管理系统}")
	private String loginTitle;
	@Value("${customer.loginLogo:/resources/imgs/common/login_logo.png}")
	private String loginLogo;
	@Value("${customer.indexTitle:管理系统}")
	private String indexTitle;
	@Value("${customer.indexLogo:/resources/imgs/common/header_logo.png}")
	private String indexLogo;
	@Value("${customer.copyText:图灵谷（北京）信息科技有限公司  @copy 2013}")
	private String copyText;

	/**
	 * 得到登录页面信息
	 */
	public Map<String, Object> getLoginInfo() {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("loginTtile", loginTitle);
		result.put("loginLogo", loginLogo);
		return result;
	}

	/**
	 * 得到主页面基本信息
	 */
	public Map<String, Object> getIndexInfo() {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("indexTitle", indexTitle);
		result.put("indexLogo", indexLogo);
		result.put("copyText", copyText); // 底部版权信息
		result.put("currentUserId", SystemHelper.getCurrentUserId()); // 当前用户id
		result.put("currentUserName", SystemHelper.getCurrentUserRealname()); // 当前用户真实姓名
		return result;
	}
}
