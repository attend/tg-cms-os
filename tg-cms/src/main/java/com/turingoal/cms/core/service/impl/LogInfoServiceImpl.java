package com.turingoal.cms.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.turingoal.cms.core.domain.LogInfo;
import com.turingoal.cms.core.domain.query.LogInfoQuery;
import com.turingoal.cms.core.repository.LogInfoDao;
import com.turingoal.cms.core.service.LogInfoService;
import com.turingoal.common.annotation.MethodLog;

/**
 * LoginfoService
 */
@Service
public class LogInfoServiceImpl implements LogInfoService {
	@Autowired
	private LogInfoDao loginfoDao;

	/**
	 * 分页查询 Loginfo
	 */
	@MethodLog(name = "分页查询Loginfo", description = "根据条件分页查询Loginfo")
	public Page<LogInfo> findByPage(final LogInfoQuery query) {
		PageHelper.startPage(query.getPage().intValue(), query.getLimit().intValue());
		return (Page<LogInfo>) loginfoDao.find(query);
	}
}