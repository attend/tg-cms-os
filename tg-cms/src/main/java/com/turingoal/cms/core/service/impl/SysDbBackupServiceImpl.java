package com.turingoal.cms.core.service.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jodd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.turingoal.cms.core.domain.DbBackup;
import com.turingoal.cms.core.domain.form.DbBackupForm;
import com.turingoal.cms.core.repository.DbBackupDao;
import com.turingoal.cms.core.service.SysDbBackupService;
import com.turingoal.common.annotation.MethodLog;
import com.turingoal.common.constants.ConstantDateFormatTypes;

/**
 * 数据库备份ServiceImpl
 */
@Service
public class SysDbBackupServiceImpl implements SysDbBackupService {
	private static final String NEWLINE = "\r\n";
	@Autowired
	private DbBackupDao dbBackupDao;

	/**
	 * 获得Schema
	 */
	public String getSchema() {
		return dbBackupDao.getSchema();
	}

	/**
	 * 获得所有表名
	 */
	public List<String> getTables(final String schema) {
		return dbBackupDao.getTables(schema);
	}

	/**
	 * 获得TableDDL
	 */
	public String getTableDDL(final String schema, final String table) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("schema", schema);
		map.put("table", table);
		String ddl = "DROP TABLE IF EXISTS " + table + ";" + NEWLINE;
		ddl = ddl + dbBackupDao.getTableDDL(map).get("Create Table") + ";" + NEWLINE;
		return ddl;
	}

	/**
	 * 获得getDataByTable
	 */
	public List<Map<String, Object>> getDataByTable(final String schema, final String table) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("schema", schema);
		map.put("table", table);
		return dbBackupDao.getTableData(map);
	}

	/**
	 * writeTableData
	 * 
	 * @throws IOException
	 */
	public void writeTableData(final String schema, final String table, final BufferedWriter writer) throws IOException {
		List<Map<String, Object>> data = getDataByTable(schema, table);
		if (data != null && data.size() > 0) {
			int count = data.size();
			String[] keys = (String[]) data.get(0).keySet().toArray(new String[0]);
			int keyLength = keys.length;
			for (int i = 0; i < count; i++) {
				Map<String, Object> record = data.get(i);
				writer.write("INSERT INTO " + schema + '.' + table + " (");
				for (int k = 0; k < keyLength; k++) {
					writer.write(keys[k]);
					if (k < keyLength - 1) {
						writer.write(',');
					}
				}
				writer.write(") VALUES (");
				for (int m = 0; m < keyLength; m++) {
					Object obj = record.get(keys[m]);
					if (obj == null) {
						writer.write("NULL");
					} else if (obj instanceof Number) {
						writer.write(obj.toString());
					} else if (obj instanceof Date) {
						SimpleDateFormat df = new SimpleDateFormat(ConstantDateFormatTypes.YYYY_MM_DD_HH24_MM_SS);
						writer.write("'");
						writer.write(df.format((Date) obj));
						writer.write("'");
					} else {
						writer.write("'");
						String s = obj.toString();
						s = StringUtil.replace(s, "\r", "\\r");
						s = StringUtil.replace(s, "\n", "\\n");
						s = StringUtil.replace(s, "'", "''");
						writer.write(s);
						writer.write("'");
					}
					if (m < keyLength - 1) {
						writer.write(',');
					}
				}
				writer.write(");");
				writer.write(NEWLINE);
			}
			writer.write(NEWLINE);
		}
	}

	/**
	 * 备份数据库
	 */
	public void backup(final File file, final boolean dataOnly) throws IOException {
		String schema = getSchema(); // schema
		List<String> tables = getTables(schema); // 所有表名
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
		writer.write("/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;" + NEWLINE);
		writer.write("/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;" + NEWLINE);
		writer.write("/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;" + NEWLINE);
		writer.write("/*!40101 SET NAMES utf8 */;" + NEWLINE);
		writer.write("SET FOREIGN_KEY_CHECKS=0;" + NEWLINE);
		if (!dataOnly) {
			for (String table : tables) {
				writer.write(getTableDDL(schema, table));
			}
		}
		for (String table : tables) {
			writeTableData(schema, table, writer);
		}
		writer.write("/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;" + NEWLINE);
		writer.write("/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;" + NEWLINE);
		writer.write("/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;" + NEWLINE);
		writer.flush();
		writer.close();
	}

	/**
	 * 查询全部 DbBackup
	 */
	@MethodLog(name = "查询全部DbBackup", description = "根据条件查询全部的DbBackup，不分页")
	public List<DbBackup> findAll() {
		return dbBackupDao.find();
	}

	/**
	 * 新增 DbBackup
	 */
	@MethodLog(name = "新增DbBackup", description = "新增一个DbBackup")
	public void add(final DbBackupForm form) throws IOException {
		// form.setCreateDataUsername(SystemHelper.getCurrentUsername());
		boolean dataOnly = true;
		File dir = new File("D:/backup");
		if (!dir.exists()) {
			dir.mkdirs();
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		Date backupDate = new Date();
		String filename = "tgcms-" + df.format(backupDate) + (dataOnly ? "-data" : "") + ".sql";
		File file = new File(dir, filename);
		form.setBackupDate(backupDate);
		backup(file, true);
		dbBackupDao.add(form);
	}

	/**
	 * 根据id删除一个 DbBackup
	 */
	@MethodLog(name = "删除DbBackup", description = "根据id删除一个DbBackup")
	public int delete(final String id) {
		return dbBackupDao.delete(id);
	}
}
