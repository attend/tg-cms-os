package com.turingoal.cms.core.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.turingoal.cms.core.commons.ShiroPasswordHelper;
import com.turingoal.cms.core.commons.SystemHelper;
import com.turingoal.cms.core.commons.SystemLogHelper;
import com.turingoal.cms.core.domain.Role;
import com.turingoal.cms.core.domain.User;
import com.turingoal.cms.core.domain.form.UserForm;
import com.turingoal.cms.core.domain.form.UserRoleForm;
import com.turingoal.cms.core.domain.query.UserQuery;
import com.turingoal.cms.core.repository.UserDao;
import com.turingoal.cms.core.repository.UserRoleDao;
import com.turingoal.cms.core.service.UserService;
import com.turingoal.common.annotation.MethodLog;
import com.turingoal.common.constants.ConstantAvailableValue;
import com.turingoal.common.constants.ConstantEditableValue;
import com.turingoal.common.constants.ConstantSystemValues;
import com.turingoal.common.exception.BusinessException;
import com.turingoal.common.exception.ExceptionCode;
import com.turingoal.common.util.lang.StringUtil;
import com.turingoal.common.util.math.RandomUtil;

/**
 * 用户Service
 */
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;
	@Autowired
	private UserRoleDao userRoleDao;
	@Autowired
	private ConfigCustomServiceImpl customConfigService;

	/**
	 * 获取登录页面信息
	 */
	@MethodLog(name = "获取登录页面信息", description = "获取登录页面信息")
	public Map<String, Object> loginInfo() {
		Map<String, Object> loginInfo = customConfigService.getLoginInfo();
		loginInfo.put("showCaptchaCode", SystemHelper.getSessionAttibute("showCaptchaCode")); // 是否需要输入验证码
		return loginInfo;
	}

	/**
	 * 登录系统
	 */
	public Map<String, Object> login(final String username, final String password) {
		// 用于存储登录结果和提示信息
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", false);
		if (StringUtil.isNullOrBlank(username) || StringUtil.isNullOrBlank(password)) {
			result.put("msg", "用户名或密码不能为空,请重新输入！");
		} else {
			Subject subject = SecurityUtils.getSubject();
			UsernamePasswordToken token = new UsernamePasswordToken(username, password);
			try {
				subject.login(token);
				User sysUserInfo = getLoginUser(username, password);
				SystemHelper.setCurrentUser(sysUserInfo); // 保存当前用户登录信息
				// 修改用户登录信息
				UserForm userForm = new UserForm();
				userForm.setId(sysUserInfo.getId());
				userForm.setLastLoginTime(new Date()); // 修改时间
				userForm.setLastLoginIp(SystemHelper.getCurrentUserIp());
				userDao.updateCurrentUserLoginInfo(userForm);
				result.put("success", true);
				result.put("msg", "登录成功");
			} catch (UnknownAccountException uae) {
				result.put("msg", "该用户名不存在！");
			} catch (ExcessiveAttemptsException eae) {
				result.put("msg", "密码错误次数太多！请确认用户名和密码！");
				result.put("showCaptchaCode", true); // 需要输入验证码
			} catch (IncorrectCredentialsException ice) {
				result.put("msg", "认证信息不正确");
			} catch (LockedAccountException lae) {
				result.put("msg", "密码错误次数太多，账号已锁定，请稍后再试！");
			} catch (DisabledAccountException lae) {
				result.put("msg", "你的账户已被禁用,请联系管理员开通！");
			} catch (AuthenticationException ae) {
				result.put("msg", "认证失败，请重新登录！");
			}
		}
		if ((boolean) result.get("success")) {
			SystemLogHelper.loginLog(username, "登录成功");
		} else {
			SystemLogHelper.loginLog(username, "登录失败," + result.get("msg"));
		}
		return result;
	}

	/**
	 * 退出系统
	 */
	@MethodLog(name = "退出系统", description = "退出系统")
	public void logout() {
		SystemHelper.logout();
	}

	/**
	 * 修改密码
	 */
	@MethodLog(name = "修改用户密码", description = "修改用户密码")
	public int updateUserPass(final String userPass) {
		Map<String, Object> params = new HashMap<String, Object>();
		String salt = RandomUtil.generateLowerString(6);
		params.put("id", SystemHelper.getCurrentUserId());
		params.put("userSalt", salt);
		params.put("userPass", ShiroPasswordHelper.encodePassword(userPass, SystemHelper.getCurrentUsername(), salt)); // 密码加密
		return userDao.updateUserPass(params);
	}

	/**
	 * 重置用户密码
	 */
	@MethodLog(name = "重置用户密码", description = "重置用户密码")
	public int resetUserPass(final String id) {
		String username = userDao.get(id).getUsername();
		Map<String, Object> params = new HashMap<String, Object>();
		String salt = RandomUtil.generateLowerString(6);
		params.put("id", id);
		params.put("userSalt", salt);
		params.put("userPass", ShiroPasswordHelper.encodePassword(ConstantSystemValues.INIT_PASS, username, salt)); // 密码加密
		return userDao.updateUserPass(params);
	}

	/**
	 * 通过用户名得到用户的id 及登录信息
	 */
	@MethodLog(name = "通过用户名得到用户的id及登录信息", description = "通过用户名得到用户的id及登录信息")
	public User getLoginInfoByUsername(final String username) {
		return userDao.findByUsername(username);
	}

	/**
	 * 获取登录用户
	 */
	@MethodLog(name = "用户登录", description = "用户登录")
	public User getLoginUser(final String username, final String userPass) {
		Map<String, Object> params = new HashMap<String, Object>();
		User user = userDao.findByUsername(username);
		if (user != null) {
			String salt = userDao.findByUsername(username).getUserSalt();
			params.put("username", username);
			params.put("userPass", ShiroPasswordHelper.encodePassword(userPass, username, salt)); // 密码加密
			return userDao.login(params);
		} else {
			return null;
		}
	}

	/**
	 * 分页查询 User
	 */
	@MethodLog(name = "分页查询 User", description = "分页查询 User")
	public Page<User> findByPage(final UserQuery query) {
		PageHelper.startPage(query.getPage().intValue(), query.getLimit().intValue());
		return (Page<User>) userDao.find(query);
	}

	/**
	 * 查询某个用户下的角色id
	 */
	@MethodLog(name = "查询某个用户下的角色id", description = "查询某个用户下的角色id")
	public List<String> getRoleIdsByUser(final String userId) {
		return userRoleDao.getRoleIdsByUser(userId);
	}

	/**
	 * 查询某个用户下的所有角色信息
	 */
	@MethodLog(name = "查询某个用户下的所有角色信息", description = "查询某个用户下的所有角色信息")
	public List<Role> getRolesByUser(final String userId) {
		return userRoleDao.getRolesByUser(userId);
	}

	/**
	 * 更新某个用户下的角色
	 */
	public boolean updateRolesByUser(final String userId, final String roleIds) throws BusinessException {
		if (userDao.checkEditable(userId) != ConstantEditableValue.EDITABLE_INT) {
			throw new BusinessException(ExceptionCode.CODE_1000);
		} else {
			userRoleDao.deleteByUserId(userId);
			String[] roleIdArray = roleIds.split(",");
			for (String roleId : roleIdArray) {
				UserRoleForm urForm = new UserRoleForm();
				urForm.setUserId(userId);
				urForm.setRoleId(roleId);
				urForm.setCreateDataUsername(SystemHelper.getCurrentUsername());
				userRoleDao.addUserRole(urForm);
			}
			return true;
		}
	}

	/**
	 * 通过id得到一个 User
	 */
	@MethodLog(name = "得到一个User", description = "通过id得到一个 User")
	public User get(final String id) {
		return userDao.get(id);
	}

	/**
	 * 新增 User
	 */
	@MethodLog(name = "新增User", description = "新增一个User")
	public void add(final UserForm form) {
		form.setCreateDataUsername(SystemHelper.getCurrentUsername());
		String userPass = form.getUserPass();
		if (StringUtil.isNullOrBlank(userPass)) {
			userPass = ConstantSystemValues.INIT_PASS; // 系统默认密码
		}
		String salt = RandomUtil.generateLowerString(6);
		form.setUserSalt(salt);
		form.setUserPass(ShiroPasswordHelper.encodePassword(userPass, form.getUsername(), salt)); // 密码加密
		userDao.add(form);
	}

	/**
	 * 修改 User
	 */
	@MethodLog(name = "修改User", description = "修改User")
	public int update(final UserForm form) throws BusinessException {
		if (userDao.checkEditable(form.getId()) != ConstantEditableValue.EDITABLE_INT) {
			throw new BusinessException(ExceptionCode.CODE_1000);
		} else {
			form.setUpdateDataUsername(SystemHelper.getCurrentUsername());
			return userDao.update(form);
		}
	}

	/**
	 * 修改当前 User
	 */
	@MethodLog(name = "修改当前User", description = "修改当前User")
	public int updateCurrentUser(final UserForm form) throws BusinessException {
		if (userDao.checkEditable(SystemHelper.getCurrentUserId()) != ConstantEditableValue.EDITABLE_INT) {
			throw new BusinessException(ExceptionCode.CODE_1000);
		} else {
			return userDao.updateCurrentUser(form);
		}
	}

	/**
	 * 根据id删除一个 User
	 */
	@MethodLog(name = "删除User", description = " 根据id删除一个 User")
	public int delete(final String id) throws BusinessException {
		if (userDao.checkEditable(id) != ConstantEditableValue.EDITABLE_INT) {
			throw new BusinessException(ExceptionCode.CODE_1000);
		} else {
			return userDao.delete(id);
		}
	}

	/**
	 * 启用
	 */
	@MethodLog(name = "启用User", description = " 根据id启用一个User")
	public int enable(final String id) throws BusinessException {
		if (userDao.checkEditable(id) != ConstantEditableValue.EDITABLE_INT) {
			throw new BusinessException(ExceptionCode.CODE_1000);
		} else {
			return userDao.changeAvailable(id, ConstantAvailableValue.AVAILABLE_INT);
		}
	}

	/**
	 * 停用
	 */
	@MethodLog(name = "停用User", description = " 根据id停用一个User")
	public int disable(final String id) throws BusinessException {
		if (userDao.checkEditable(id) != ConstantEditableValue.EDITABLE_INT) {
			throw new BusinessException(ExceptionCode.CODE_1000);
		} else {
			return userDao.changeAvailable(id, ConstantAvailableValue.UNAVAILABLE_INT);
		}
	}
}