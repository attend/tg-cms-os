package com.turingoal.cms.core.web.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.baidu.ueditor.ActionEnter;
import com.turingoal.cms.core.commons.SystemHelper;
import com.turingoal.cms.core.repository.LogInfoDao;
import com.turingoal.cms.core.service.ConfigCustomService;
import com.turingoal.cms.core.service.MonitorSysinfoService;
import com.turingoal.cms.core.service.ResourceService;
import com.turingoal.cms.core.service.UserService;
import com.turingoal.cms.modules.base.domain.Global;
import com.turingoal.cms.modules.base.domain.query.GlobalQuery;
import com.turingoal.cms.modules.base.service.GlobalService;
import com.turingoal.common.util.math.CaptchaUtil;

/**
 * 系统登录，退出
 */
@Controller
public class SysLoginController {
	@Autowired
	private UserService userService;
	@Autowired
	private ConfigCustomService customConfigService;
	@Autowired
	private ResourceService resourceService;
	@Autowired
	private GlobalService globalService;
	@Autowired
	private LogInfoDao loginfoDao;
	@Autowired
	private MonitorSysinfoService sysinfoService;

	/**
	 * 跳转到登录页面
	 */
	@RequestMapping(value = "/loginPage.gsp", method = RequestMethod.GET)
	public String loginPage() {
		return "login";
	}

	/**
	 * 登录系统
	 */
	@RequestMapping(value = "/login.gsp", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> login(@RequestParam final String username, @RequestParam final String password) {
		return userService.login(username, password);
	}

	/**
	 * 跳转到无权限页面
	 */
	@RequestMapping(value = "/noAuth.gsp")
	public String noAuth() {
		return "noAuth";
	}

	/**
	 * 获取验证码
	 */
	@RequestMapping(value = "/getCaptcha.gsp")
	public void getImage(final HttpSession session, final HttpServletResponse response) {
		// 生成验证码并放到Session中
		CaptchaUtil.getCaptcha(session, response);
	}

	/**
	 * 获取登录页面信息
	 */
	@RequestMapping(value = "/loginInfo.gsp")
	@ResponseBody
	public Map<String, Object> loginInfo() {
		return userService.loginInfo();
	}

	/**
	 * 退出系统
	 */
	@RequestMapping(value = "/logout.gsp")
	public String logout() {
		userService.logout();
		return "redirect:admin/index.gsp";
	}

	/**
	 * 后台主页
	 */
	@RequestMapping(value = "/admin")
	public String admin() {
		return "redirect:admin/index.gsp";
	}

	/**
	 * 跳转到主页面
	 */
	@RequestMapping(value = "/index.gsp")
	public ModelAndView index(final HttpServletRequest request) {
		ModelAndView mav;
		if (SystemHelper.isAuthenticated()) {
			mav = new ModelAndView("index");
			if (SystemHelper.getGlobal() == null) {
				List<Global> gs = globalService.findAll(new GlobalQuery());
				SystemHelper.setGlobal(gs.get(0));
			}
			SystemHelper.setSessionAttibute("auths", resourceService.findPermissionsEnabledByUser(SystemHelper.getCurrentUsername()));
			mav.addObject("resultList", sysinfoService.getInfo(request));
			return mav;
		} else {
			mav = new ModelAndView("redirect:/admin/loginPage.gsp");
			return mav;
		}
	}

	/**
	 * ueditor上传
	 */
	@RequestMapping(value = "/resources/vendor/ueditor/jsp/controller.gsp")
	@ResponseBody
	public void controller(final HttpServletRequest request, final HttpServletResponse response) {
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		ActionEnter ae = new ActionEnter(request, rootPath);
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("utf-8");
		try {
			PrintWriter writer = response.getWriter();
			writer.write(ae.exec());
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 将form表单里面的字符串去掉空白
	 */
	@InitBinder
	protected void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
}
