package com.turingoal.cms.modules.base.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.turingoal.cms.core.commons.SystemHelper;
import com.turingoal.cms.modules.base.domain.Attr;
import com.turingoal.cms.modules.base.domain.form.AttrForm;
import com.turingoal.cms.modules.base.domain.query.AttrQuery;
import com.turingoal.cms.modules.base.repository.AttrDao;
import com.turingoal.cms.modules.base.service.AttrService;
import com.turingoal.common.annotation.MethodLog;

/**
 * 属性Service
 */
@Service
public class AttrServiceImpl implements AttrService {
	@Autowired
	private AttrDao attrDao;

	/**
	 * 查询全部 属性
	 */
	@MethodLog(name = "查询全部属性", description = "根据条件查询全部的属性，不分页")
	public List<Attr> findAll(final AttrQuery query) {
		return attrDao.find(query);
	}

	/**
	 * 通过codeNum得到一个 属性
	 */
	@MethodLog(name = "通过codeNum得到一个 属性", description = "通过codeNum得到一个 属性")
	public Attr getByCode(final String codeNum) {
		return attrDao.getByCode(codeNum);
	}

	/**
	 * 通过id得到一个 属性
	 */
	@MethodLog(name = "通过id得到属性", description = "通过id得到一个属性")
	public Attr get(final String id) {
		Attr attr = attrDao.get(id);
		attr.setImagePath(SystemHelper.getGlobal().getContextPath() + attr.getImagePath());
		return attr;
	}

	/**
	 * 根据文章id获取属性
	 */
	@MethodLog(name = "根据文章id获取属性", description = "根据文章id获取属性")
	public List<Attr> findByInfoId(final String id) {
		List<Attr> attrs = attrDao.findByInfoId(id);
		for (Attr attr : attrs) {
			attr.setImagePath(SystemHelper.getGlobal().getContextPath() + attr.getImagePath());
		}
		return attrs;
	}

	/**
	 * 新增 属性
	 */
	@MethodLog(name = "新增属性", description = "新增一个属性")
	public void add(final AttrForm form) {
		Integer priority = attrDao.getMaxPriority(form);
		form.setPriority(priority == null ? 1 : priority + 1);
		form.setCreateDataUsername(SystemHelper.getCurrentUsername());
		attrDao.add(form);
	}

	/**
	 * 修改 属性
	 */
	@MethodLog(name = "修改属性", description = "修改一个属性")
	public int update(final AttrForm form) {
		form.setUpdateDataUsername(SystemHelper.getCurrentUsername());
		return attrDao.update(form);
	}

	/**
	 * 根据id删除一个 Attr
	 */
	@MethodLog(name = "删除属性", description = "根据id删除一个属性")
	public int delete(final String id) {
		attrDao.changePrioritysByDelete(id);
		return attrDao.delete(id);
	}

	/**
	 * 属性管理排序
	 */
	@MethodLog(name = "属性管理排序", description = "修改属性管理排序")
	public int updateOrder(final Integer oldIndex, final Integer newIndex) {
		AttrForm form = new AttrForm();
		Integer maxPriority = attrDao.getMaxPriority(form);
		Integer oldPriority = maxPriority - oldIndex; // 原来的优先级 3
		Integer newPriority = maxPriority - newIndex; // 新的优先级 6
		// 修改当前数据的优先级 先临时保存为0
		form.setNewPriority(0);
		form.setOldPriority(oldPriority);
		attrDao.changePriority(form);
		// 上移，中间部分减少优先级
		if (oldIndex > newIndex) {
			form.setStartPriority(oldPriority + 1);
			form.setEndPriority(newPriority);
			attrDao.decreasePrioritys(form);
		} else { // 下移，中间部分增大优先级
			form.setStartPriority(newPriority);
			form.setEndPriority(oldPriority - 1);
			attrDao.increasePrioritys(form);
		}
		// 修改当前数据的优先级
		form.setNewPriority(newPriority);
		form.setOldPriority(0);
		return attrDao.changePriority(form);
	}
}