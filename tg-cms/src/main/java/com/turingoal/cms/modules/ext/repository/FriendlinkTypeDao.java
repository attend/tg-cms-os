package com.turingoal.cms.modules.ext.repository;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.turingoal.cms.modules.ext.domain.FriendlinkType;
import com.turingoal.cms.modules.ext.domain.form.FriendlinkTypeForm;
import com.turingoal.cms.modules.ext.domain.query.FriendlinkTypeQuery;

/**
 * 友情链接类型Dao
 */
public interface FriendlinkTypeDao {

	/**
	 * 查询 友情链接类型
	 */
	List<FriendlinkType> find(final FriendlinkTypeQuery query);

	/**
	 * 通过id得到一个 友情链接类型
	 */
	FriendlinkType get(final String id);

	/**
	 * 新增 友情链接类型
	 */
	void add(final FriendlinkTypeForm form);

	/**
	 * 修改 友情链接类型
	 */
	int update(final FriendlinkTypeForm form);

	/**
	 * 根据id删除一个 友情链接类型
	 */
	int delete(final String id);

	/**
	 * 得到最大优先级
	 */
	int getMaxPriority(final FriendlinkTypeForm form);

	/**
	 * 修改优先级
	 */
	int changePriority(final FriendlinkTypeForm form);

	/**
	 * 递增优先级
	 */
	int increasePrioritys(final FriendlinkTypeForm form);

	/**
	 * 递减优先级
	 */
	int decreasePrioritys(final FriendlinkTypeForm form);

	/**
	 * 删除数据，更新其它数据优先级
	 */
	int changePrioritysByDelete(@Param("id") final String id);
}