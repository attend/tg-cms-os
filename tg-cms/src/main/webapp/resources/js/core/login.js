$(function() {
	var showCaptchaCode = false;

	$("#captchaImg").click(function() {
				$(this).attr("src", "getCaptcha.gsp?a=" + new Date());
			});
	$.ajax({
				type : "POST",
				url : "loginInfo.gsp",
				success : function(responseText) {
					var result = jQuery.parseJSON(responseText);
					showCaptchaCode = result.showCaptchaCode;
					// 是否显示验证码
					if (result.showCaptchaCode == true) {
						$("#captchaCodeDiv").show();
					}
				}
			});

	var validator = $("#loginForm").validate({
		/*
		 * highlight : function(element) {
		 * $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); },
		 * success : function(element) {
		 * $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); },
		 */
		errorLabelContainer : "#showErrorMsg",
		rules : {
			username : {
				required : true,
				minlength : 3,
				maxlength : 25
			},
			password : {
				required : true,
				minlength : 6,
				maxlength : 25
			},
			captchaCode : {
				required : true,
				maxlength : 5,
				minlength : 5
			}
		},
		messages : {
			username : {
				required : "请输入您的用户名.",
				minlength : "用户名必须3个字符以上.",
				maxlength : "用户名不能大于25个字符."
			},
			password : {
				required : "请输入您的密码.",
				minlength : "密码必须6个字符以上.",
				maxlength : "密码不能大于25个字符."
			},
			captchaCode : {
				required : "请输入您的验证码.",
				minlength : "验证码必须5个字符.",
				maxlength : "验证码必须5个字符."
			}
		},
		submitHandler : function(form) {
			$.ajax({
				type : "POST",
				url : "login.gsp",
				data : $(form).serialize(),
				success : function(responseText) {
					var result = jQuery.parseJSON(responseText);
					if (result.success) {
						window.location.href = 'index.gsp';
					} else {
						$("#showErrorMsg")
								.html('<label class="error" style="display: inline-block;">'
										+ result.msg + '</label>');
						$("#showErrorMsg").show();
						// 是否显示验证码
						if (result.showCaptchaCode || showCaptchaCode) {
							$("#captchaCodeDiv").show();
						} else {
							$("#captchaCodeDiv").hide();
						}
					}
				}
			});
		}
	});
	$("#resetBtn").click(function() {
				$(".has-error").removeClass('has-error');
				validator.resetForm(); 
			});
});
